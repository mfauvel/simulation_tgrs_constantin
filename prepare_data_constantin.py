# coding: utf-8
import otbApplication
import os
import argparse
import sqlite3
import pandas as pd
import numpy as np
import logging
import xml.etree.ElementTree as ET
from osgeo import gdal, ogr



def create_raw_data_cube(TILE="T31TCJ", debug=False, random_state=0,
                         write_outputs_masks=False, write_outputs_images=False):
    """Create raw data cube from iota2 stack"""

    # Parameters
    s2_path = "/work/OT/theia/oso/vincent/Teruti/data/S2/2018/"
    tile_path = os.path.join(s2_path, TILE)
    
    # Get list of repertory in ascendent order for the current tile
    list_dirs = os.listdir(tile_path)
    list_dirs = sorted(list_dirs,
                       key=lambda dir_:dir_.split("_")[1].split("-")[0])

    # Create list of image and mask to stack
    list_images = []
    list_masks = []
    for dir_ in list_dirs:
        # Images
        im_path = os.path.join(
            tile_path,
            dir_,
            "{}_FRE_STACK.tif".format(dir_))
        # Mask
        mask_path = os.path.join(
            tile_path,
            dir_,
            "MASKS",
            "{}_BINARY_MASK.tif".format(dir_))

        # Add to list
        list_images.append(im_path)
        list_masks.append(mask_path)

    if debug is True:
        print(list_images)
        print("\n")
        print(list_masks)
    
    # Save dates for later gapfilling
    with open("/work/scratch/fauvelm/SampleExtraction/outputs"
              "/{}/TimeSeriesInputDateFile_{}.txt".format(TILE, random_state),"w") as the_file:
        for dir_ in list_dirs:
            the_file.write("{}\n".format(
                dir_.split("_")[1][:8])
                        )

    # Create OTB application for images concatetation
    concat_images = otbApplication.Registry.CreateApplication("ConcatenateImages")
    concat_images.SetParameterStringList("il", list_images)
    concat_images.SetParameterString("out",
                                     "/work/scratch/fauvelm/SampleExtraction/outputs"
                                     "/{}/data_cube.tif".format(TILE))
    if write_outputs_images:
        concat_images.ExecuteAndWriteOutput()
    else:
        concat_images.Execute()

    # Create OTB application for masks concatetation
    concat_masks = otbApplication.Registry.CreateApplication("ConcatenateImages")
    concat_masks.SetParameterStringList("il", list_masks)
    concat_masks.SetParameterString("out",
                                    "/work/scratch/fauvelm/SampleExtraction/outputs"
                                    "/{}/mask_cube.tif".format(TILE))

    if write_outputs_masks:
        concat_masks.ExecuteAndWriteOutput()
    else:
        concat_masks.Execute()

    # Return 
    if write_outputs_images and write_outputs_masks:    
        return concat_images.GetParameters()["out"], concat_masks.GetParameters()["out"]
    elif write_outputs_masks:
        return concat_images, concat_masks.GetParameters()["out"]
    else:
        return concat_images, concat_masks


def select_labeled_data(TILE="T31TCJ", concat_images=None, concat_masks=None,
                        gap_filled_images=None, max_sample=10000, random_state=0):

    output_file =  "/work/scratch/fauvelm/SampleExtraction/outputs/{}/samples_{}.sqlite".format(TILE, random_state)
    
    # PolygonClassStatistics
    polygon_stat = otbApplication.Registry.CreateApplication("PolygonClassStatistics")
    polygon_stat.SetParameterInputImage("in",
                                            concat_images.GetParameterOutputImage("out"))
    polygon_stat.SetParameterString("vec",
                                        "/work/OT/theia/oso/production/cnes/inputs/shapes/learn_CP2014_RPG2018CA23_UABDTOPO.shp")
    polygon_stat.UpdateParameters()
    polygon_stat.SetParameterString("field", "code")
    polygon_stat.SetParameterString("out",
                                        "/work/scratch/fauvelm/SampleExtraction/outputs"
                                        "/{}/stat_{}.xml".format(TILE, random_state))
    polygon_stat.ExecuteAndWriteOutput()
    polygon_stat = None

    # Define the number of samples perclass
    sample_per_class_filename = generate_number_of_sample_perclass(TILE,
                                                                   max_sample=max_sample,
                                                                   random_state=random_state)
    
    # Select sample
    sample_selection = otbApplication.Registry.CreateApplication("SampleSelection")
    sample_selection.SetParameterInputImage("in",
                                                 concat_images.GetParameterOutputImage("out"))
    sample_selection.SetParameterString("vec",
                                             "/work/OT/theia/oso/production/cnes/inputs/shapes/learn_CP2014_RPG2018CA23_UABDTOPO.shp")
    sample_selection.SetParameterString("instats",
                                             "/work/scratch/fauvelm/SampleExtraction/outputs"
                                             "/{}/stat_{}.xml".format(TILE, random_state))
    sample_selection.UpdateParameters()
    sample_selection.SetParameterString("field", "code")
    sample_selection.SetParameterString("rand", str(random_state))
    sample_selection.SetParameterString("strategy", "byclass")
    sample_selection.SetParameterString("strategy.byclass.in",
                                            sample_per_class_filename)
    sample_selection.SetParameterString("sampler", "random")
    sample_selection.SetParameterString("out",
                                            output_file)
    sample_selection.SetParameterString("outrates",
                                            "/work/scratch/fauvelm/SampleExtraction/outputs"
                                            "/{}/outrates_{}.csv".format(TILE,
                                                                         random_state))
    sample_selection.ExecuteAndWriteOutput()
    sample_selection = None

    # Add ogc_fid to columns to ensure consistency for later extract samples
    con = sqlite3.connect(output_file)
    df = pd.read_sql_query("select * from output", con)
    df["samplefid"] = df["ogc_fid"]
    df.to_sql("output", con, if_exists="replace")
    con.close()

    return output_file


def extract_labeled_data(TILE="T31TCJ",
                         concat_images=None, concat_masks=None, gap_filled_images=None,
                         samples_file=None, data_set=None, random_state=None, number_data=3):

    if number_data == 3:
        DATA_TYPE = ["sits", "mask", "sits_gp"]
    else:
        DATA_TYPE = ["mask", "sits_gp"]

    # Extract sample
    for data_type in DATA_TYPE:
        vec_file = "/work/scratch/fauvelm/SampleExtraction/outputs/{}/".format(TILE) \
            + "samples_{}_{}.sqlite".format(data_set, random_state)
        
        sample_extraction = otbApplication.Registry.CreateApplication("SampleExtraction")
        sample_extraction.SetParameterString("vec",
                                                 vec_file)
        
        sample_extraction.SetParameterString("field", "code")
        sample_extraction.SetParameterString("outfield", "prefix")
        
        if data_type == "sits":         
            sample_extraction.SetParameterInputImage("in",
                                                        concat_images.GetParameterOutputImage("out"))
            sample_extraction.SetParameterString("outfield.prefix.name", "TS_")
            sample_extraction.SetParameterString("out",
                                                     "/work/scratch/fauvelm/SampleExtraction/outputs"
                                                     "/{}/sits_{}_{}.csv".format(TILE, data_set, random_state))
        elif data_type == "mask":
            if isinstance(concat_masks, str):
                sample_extraction.SetParameterString("in",
                                                     concat_masks)
            else:
                sample_extraction.SetParameterInputImage("in",
                                                         concat_masks.GetParameterOutputImage("out"))
            sample_extraction.SetParameterString("outfield.prefix.name", "MASK_")
            sample_extraction.SetParameterString("out",
                                                 "/work/scratch/fauvelm/SampleExtraction/outputs"
                                                 "/{}/mask_{}_{}.csv".format(TILE, data_set, random_state))
        elif data_type == "sits_gp":
            sample_extraction.SetParameterInputImage("in",
                                                     gap_filled_images.GetParameterOutputImage("out"))
            sample_extraction.SetParameterString("outfield.prefix.name", "TSG_")
            sample_extraction.SetParameterString("out",
                                                 "/work/scratch/fauvelm/SampleExtraction/outputs"
                                                 "/{}/sits_gp_{}_{}.csv".format(TILE, data_set, random_state))
        sample_extraction.ExecuteAndWriteOutput()


def gap_filling(TILE="T31TCJ", concat_images=None, concat_masks=None, random_state=0):
    gap_filling = otbApplication.Registry.CreateApplication("ImageTimeSeriesGapFilling")

    gap_filling.SetParameterInputImage("in",
                                       concat_images.GetParameterOutputImage("out"))
    
    if isinstance(concat_masks, str):
        gap_filling.SetParameterString("mask",
                                       concat_masks)
    else:
        gap_filling.SetParameterInputImage("mask",
                                           concat_masks.GetParameterOutputImage("out"))

    gap_filling.SetParameterInt("comp", 10)
    gap_filling.SetParameterString("it", "linear")
    gap_filling.SetParameterString("id",
                                       "/work/scratch/fauvelm/SampleExtraction/outputs"
                                       "/{}/TimeSeriesInputDateFile_{}.txt".format(TILE,
                                                                                   random_state))
    gap_filling.SetParameterString("od", "/home/eh/fauvelm/scratch/SampleExtraction/TimeSeriesOutputDateFile.txt")

    gap_filling.Execute()

    return gap_filling


def build_noisy_mask(TILE="T31TCJ", concat_images=None, random_state=0, noise_level=0.25):
    # Open Mask
    mask_file = gdal.Open("/work/scratch/fauvelm/SampleExtraction/outputs"
                          "/{}/mask_cube.tif".format(TILE),
                          gdal.GA_ReadOnly)

    nTimeStamp = mask_file.RasterCount
    GeoTransform = mask_file.GetGeoTransform()
    xOrigin = GeoTransform[0]  # xmin
    yOrigin = GeoTransform[3]  # ymax
    pixelWidth = GeoTransform[1]
    pixelHeight = GeoTransform[5]

    # Open S2 Stack
    appExtractRoi = otbApplication.Registry.CreateApplication("ExtractROI")
    appExtractRoi.SetParameterInputImage("in",
                                         concat_images.GetParameterOutputImage("out"))
    appExtractRoi.SetParameterString("mode", "standard")
    appExtractRoi.SetParameterInt("sizex", 1)
    appExtractRoi.SetParameterInt("sizey", 1)
    appExtractRoi.Execute()
    nFeatures = appExtractRoi.GetImageNbBands("out")
    nBands = nFeatures/nTimeStamp
    channels = ["Channel{}".format(int(1 + t*nBands)) for t in range(nTimeStamp)]
    appExtractRoi.SetParameterStringList("cl", channels)
    
    # Create Noisy Mask
    driver = gdal.GetDriverByName("GTiff")
    noisy_mask_filename = "/work/scratch/fauvelm/SampleExtraction/outputs"\
        "/{}/noisy_mask_cube_{}.tif".format(TILE,
                                            random_state)
    noisy_mask_file = driver.CreateCopy(noisy_mask_filename,
                                        mask_file,
                                        strict=True)
    mask_pixel = np.empty((nTimeStamp), dtype='uint8')
    
    # Open train sqlite file
    mask_sample_file = ogr.Open("/work/scratch/fauvelm/SampleExtraction/outputs"
                                "/{}/samples_{}.sqlite".format(TILE, random_state), 0)
    
    mask_sample = mask_sample_file.GetLayer()
    for feat in mask_sample:
        geom = feat.GetGeometryRef()
        mx, my=geom.GetX(), geom.GetY()  #coord in map units

        # Convert from map to pixel coordinates: Only works for geotransforms with no rotation.
        col = int((mx - xOrigin) / pixelWidth) #x pixel
        row = int((my - yOrigin) / pixelHeight) #y pixel

        # Read input pixel
        for d in range(nTimeStamp):
            mask_pixel[d] = mask_file.GetRasterBand(d+1).ReadAsArray(xoff=col,
                                                                     yoff=row,
                                                                     win_xsize=1,
                                                                     win_ysize=1).squeeze()
        # Read S2 STACK
        appExtractRoi.SetParameterInt("startx", col)
        appExtractRoi.SetParameterInt("starty", row)
        appExtractRoi.Execute()
        stack_pixel = appExtractRoi.GetVectorImageAsNumpyArray("out").squeeze()
            
        t = np.where(np.logical_and(mask_pixel > 0,
                                    stack_pixel >=0))[0]
        if t.size > 0:
            n_choice = np.rint(t.size*noise_level).astype(int)
            t_ = np.random.choice(t, size=n_choice, replace=False)
            mask_pixel[t_] = 0

            # Write output pixel
            for d in range(nTimeStamp):
                noisy_mask_file.GetRasterBand(d+1).WriteArray(mask_pixel[d].reshape(1, 1),
                                                              xoff=col,
                                                              yoff=row)

    mask_file = None
    noisy_mask_file = None
    mask_sample_file = None
    return noisy_mask_filename


def split_sample(samples_file=None, random_state=0):
    """Split the selected samples according to the polygon membership"""
    # connect
    conn = sqlite3.connect(samples_file)
    cdir = os.path.dirname(samples_file)

    # Get the number of polygon ID
    originfids = pd.read_sql_query("SELECT DISTINCT originfid FROM output",
                                   conn).values.squeeze()

    # Split into two groups: training and validation
    np.random.seed(random_state)
    perm = np.random.permutation(originfids)
    fid_train=perm[::2]
    conn = None
    
    # Create train and validation sqlite
    vector_in = ogr.Open(samples_file)
    layer_in = vector_in.GetLayer()

    vector_out_train = ogr.GetDriverByName('sqlite').CreateDataSource(
        os.path.join(cdir,
                     'samples_train_{}.sqlite'.format(random_state)))
    
    layer_out_train = vector_out_train.CreateLayer(
        layer_in.GetName(), layer_in.GetSpatialRef(), layer_in.GetGeomType())
    
    vector_out_val = ogr.GetDriverByName('sqlite').CreateDataSource(
        os.path.join(cdir,
                     'samples_val_{}.sqlite'.format(random_state)))
    layer_out_val = vector_out_val.CreateLayer(
        layer_in.GetName(), layer_in.GetSpatialRef(), layer_in.GetGeomType())

    defn = layer_in.GetLayerDefn()

    for i in range(defn.GetFieldCount()):
        layer_out_train.CreateField(defn.GetFieldDefn(i))
        layer_out_val.CreateField(defn.GetFieldDefn(i))

    layer_out_val.StartTransaction()
    layer_out_train.StartTransaction()

    for feat in layer_in:
        if np.any(fid_train==feat.GetField("originfid")):
            layer_out_train.CreateFeature(feat)
        else:
            layer_out_val.CreateFeature(feat)

    layer_out_val.CommitTransaction()
    layer_out_train.CommitTransaction()
    

    layer_out_train = None
    layer_out_val = None
    layer_in = None
    vector_in = None
    vector_out_val = None
    vector_out_train = None


def generate_number_of_sample_perclass(TILE="T31TCJ", max_sample=10000, random_state=0):
    tree = ET.parse("/work/scratch/fauvelm/SampleExtraction/outputs"
                    "/{}/stat_{}.xml".format(TILE,
                                             random_state))
    root = tree.getroot()
    tab = []
    for child in root[0]:
        tab.append([int(child.attrib["key"]),
                    int(child.attrib["value"])])
    tab = np.asarray(tab).astype(int)
    tab[tab>max_sample]=max_sample
    filename = "/work/scratch/fauvelm/SampleExtraction/outputs/{}/sample_per_class_{}.csv".format(TILE,
                                                                                                  random_state)
    np.savetxt(filename,
               tab, fmt='%i',
               delimiter=',')
    return filename
    

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Collection of tools to prepare data"
                                     "for Alexandre Constantion PhD")
    parser.add_argument("-tile", dest="tile", help="list S2 tiles to process",
                        default="T31TCJ", required=True)
    parser.add_argument("-task", dest="task", help="Task to do",
                        default="ExtractRefSamples", required=True)
    parser.add_argument("-maxs", dest="maxs", help="Maximum number of samples per class",
                        default=30000, required=False, type=int)
    parser.add_argument("-rs", dest="rs", help="Random State",
                        default=0, required=False, type=int)
    parser.add_argument("-nl", dest="nl", help="Noise level in the mask cube",
                        default=0.25, required=False, type=float)

    args = parser.parse_args()
    logging.basicConfig(filename="/work/scratch/fauvelm/SampleExtraction/logs/log_{}_{}.log".format(args.tile, args.rs),
                        filemode="w",
                        level=logging.DEBUG)
    random_state = args.rs   

    if args.task == "ExtractRefSamples":
        # Create SITS and Mask stack in memory
        logging.info("Create SITS and Mask stack in memory")
        concat_images, concat_masks = create_raw_data_cube(TILE=args.tile,
                                                                   debug=False,
                                                                   random_state=args.rs)

        # Create GP stack of SITS stack in memory
        logging.info("Create GP stack of SITS stack in memory")
        gap_filled_images = gap_filling(TILE=args.tile,
                                            concat_images=concat_images,
                                            concat_masks=concat_masks,
                                            random_state=args.rs)

        # Select samples
        logging.info("Class statistics and samples selection")
        samples_file = select_labeled_data(TILE=args.tile,
                                           concat_images=concat_images,
                                           concat_masks=concat_masks,
                                           gap_filled_images=gap_filled_images,
                                           max_sample=args.maxs, random_state=args.rs)

        # Split into training             
        logging.info("Split samples")
        split_sample(samples_file=samples_file,
                     random_state=args.rs)

        logging.info("Extract training samples")
        extract_labeled_data(TILE=args.tile,
                             concat_images=concat_images,
                             concat_masks=concat_masks,
                             gap_filled_images=gap_filled_images,
                             samples_file=samples_file,
                             data_set="train",
                             random_state=args.rs)

        logging.info("Extract validation samples")
        extract_labeled_data(TILE=args.tile,
                             concat_images=concat_images,
                             concat_masks=concat_masks,
                             gap_filled_images=gap_filled_images,
                             samples_file=samples_file,
                             data_set="val",
                             random_state=args.rs)
        logging.info("Extraction terminated")

    elif args.task == "BuildNoisyGPSits":
        logging.info("Build Mask Data Cube")
        concat_images, concat_masks = create_raw_data_cube(TILE=args.tile,
                                                           debug=False, write_outputs_masks=True,
                                                           random_state=args.rs)

        logging.info("Add non clouds detection / noise in the mask data cube")
        noisy_mask_filename = build_noisy_mask(TILE=args.tile,
                                               concat_images=concat_images,
                                               random_state=args.rs,
                                               noise_level=args.nl)

        logging.info("Create GP stack of SITS stack in memory with noisy data cube")
        gap_filled_images = gap_filling(TILE=args.tile,
                                        concat_images=concat_images,
                                        concat_masks=noisy_mask_filename,
                                        random_state=args.rs)

        logging.info("Initialize sample sqlite file from previous extraction")
        samples_file = "/work/scratch/fauvelm/SampleExtraction/outputs/{}/samples_{}.sqlite".format(args.tile, args.rs)

        logging.info("Extract training samples")
        extract_labeled_data(TILE=args.tile,
                             concat_images=concat_images,
                             concat_masks=noisy_mask_filename,
                             gap_filled_images=gap_filled_images,
                             samples_file=samples_file,
                             data_set="train",
                             random_state=args.rs,
                             number_data=2)

        logging.info("Extract validation samples")
        extract_labeled_data(TILE=args.tile,
                             concat_images=concat_images,
                             concat_masks=noisy_mask_filename,
                             gap_filled_images=gap_filled_images,
                             samples_file=samples_file,
                             data_set="val",
                             random_state=args.rs,
                             number_data=2)
        logging.info("Extraction terminated")

        # Rename file according to noise_level
        logging.info("Conversion CSV -> HDF")
        for data_set in ["train", "val"]:
            for name in ["mask", "sits_gp"]:
                file_in = "/work/scratch/fauvelm/SampleExtraction/outputs/{0}/{1}_{2}_{3}.csv".format(args.tile,
                                                                                                      name,
                                                                                                      data_set,
                                                                                                      args.rs)
                file_out = "/work/scratch/fauvelm/SampleExtraction/outputs/{0}/{1}_{2}_{3}_{4}.hdf".format(args.tile,
                                                                                                           name,
                                                                                                           data_set,
                                                                                                           args.rs,
                                                                                                           str(args.nl).replace(".", "_"))
                pd.read_csv(file_in).to_hdf(file_out, "output", mode="w", format="table")
                os.remove(file_in)

        # Clean repertory
        logging.info("Remove Noise Mask Data Cube")
        os.remove("/work/scratch/fauvelm/SampleExtraction/outputs/{}/mask_cube.tif".format(args.tile))
        os.remove("/work/scratch/fauvelm/SampleExtraction/outputs/{}/noisy_mask_cube_{}.tif".format(args.tile, args.rs))
