import pandas as pd
import glob
import os

def conversion(val_in, dict_code):
    return dict_code[val_in]


TILES = ["T31TCJ", "T31TDN", "T31TGK"]

# Load file for convertion
df_code = pd.read_csv("/work/scratch/fauvelm/SampleExtraction/nomenclature5niv.csv")

for TILE in TILES:
    # Get all the SITS hdf files
    HDF_FILES = glob.glob("/work/scratch/fauvelm/SampleExtraction/"
                          "outputs/{}/sits_*.hdf".format(TILE))

    # Open them iteratively
    for hdf_file in HDF_FILES:
        df = pd.read_hdf(hdf_file)

        # Add the sub codes
        for code in [1, 2, 3, 4]:
            dict_code = {val_in:val_out for val_in, val_out
                         in zip(df_code["Code_num.5"],
                                df_code["Code_num.{}".format(code)])}

            # Apply conversion
            df["code_{}".format(code)] = df["code"].apply(conversion, args=[dict_code])

        # Save data (erase old one)
        df.to_hdf(hdf_file, 'output', mode='w', format='table')
        print("Tile: {} - File {} done".format(TILE, hdf_file))
