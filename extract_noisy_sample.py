import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# From samples_train_0
samplefid_clouds = [711510, 903640]

# Defines bands
band_mask = ["MASK_{}".format(nt) for nt in range(93)]
band_raw = [["TS_{}".format(nt*10 + band) for nt in range(93)] for band in range(10)]
band_gp = [["TSG_{}".format(nt*10 + band) for nt in range(73)] for band in range(10)]

# Load data
band = 0
df_raw = pd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/sits_train_0.hdf").set_index("samplefid")[band_raw[band]]
df_gp = pd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/sits_gp_train_0.hdf").set_index("samplefid")[band_gp[band]]
df_mask = pd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/mask_train_0.hdf").set_index("samplefid")[band_mask]

# Load time samples
# Load and format date
timeSample = np.loadtxt("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/TimeSeriesInputDateFile_0.txt").astype(str)
date_in = pd.DatetimeIndex([dt.datetime(int(t[:4]), int(t[4:6]), int(t[6:8])) \
                            for t in timeSample])
timeSample = np.loadtxt("/work/scratch/fauvelm/SampleExtraction/TimeSeriesOutputDateFile.txt").astype(str)
date_out = pd.DatetimeIndex([dt.datetime(int(t[:4]), int(t[4:6]), int(t[6:8])) \
                             for t in timeSample])
# Get Indices
with PdfPages("visu_samples_{}_unmasked.pdf".format(band)) as pdf:
    for fid in samplefid_clouds:
        try:
            plt.figure()
            plt.grid()
            plt.plot(date_out, df_gp.loc[fid, :],"r")
            t = df_raw.loc[fid] > 0
            plt.scatter(date_in[t], df_raw.loc[fid, t], c=df_mask.loc[fid, :].values[t],
                       label=["Cloudy", "Clean"])
            plt.legend()
            plt.title("Sample non detected: {}".format(fid))
            pdf.savefig()
            plt.close()
        except:
            print("FID {} not the index".format(fid))

    # Write Metadata
    d = pdf.infodict()
    d['Title'] = 'Extracted noisy samples for TGRS papers'
    d['Author'] = 'Mathieu Fauvel'
    d['CreationDate'] = dt.datetime.today()

