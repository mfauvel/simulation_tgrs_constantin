import pandas as pd
import dask
import dask.dataframe as dd
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Get column names for the blue band
band_dic = {0:"blue", 1:"green", 2:"red", 3:"red_edge_1", 4:"red_edge_2",
            5:"red_edge_3", 6:"nir", 7:"nnir", 8:"swir1", 9:"swir2"}
BAND = 0
band_mask = ["MASK_{}".format(nt) for nt in range(93)]
band_blue = ["TS_{}".format(nt*10 + BAND) for nt in range(93)]
band_blue_gp = ["TSG_{}".format(nt*10 + BAND) for nt in range(73)]

# Open data
SITS = dd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/sits_train_0.hdf", "output", chunksize=40000,
                   columns=band_blue + ["samplefid", "code"]).set_index("samplefid")
MASK = dd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/mask_train_0.hdf", "output", chunksize=40000,
                   columns=band_mask + ["samplefid", "code"]).set_index("samplefid")
GP = dd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/sits_gp_train_0.hdf", "output", chunksize=40000,
                 columns=band_blue_gp + ["samplefid", "code"]).set_index("samplefid")
MASK_NOISE = dd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/mask_train_0_0_25.hdf", "output", chunksize=40000,
                         columns=band_mask + ["samplefid", "code"]).set_index("samplefid")
GP_NOISE = dd.read_hdf("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/sits_gp_train_0_0_25.hdf", "output", chunksize=40000,
                       columns=band_blue_gp + ["samplefid", "code"]).set_index("samplefid")

# Load and format date
timeSample = np.loadtxt("/work/scratch/fauvelm/SampleExtraction/outputs/T31TCJ/TimeSeriesInputDateFile_0.txt").astype(str)
date_in = pd.DatetimeIndex([dt.datetime(int(t[:4]), int(t[4:6]), int(t[6:8])) \
                            for t in timeSample])
timeSample = np.loadtxt("/work/scratch/fauvelm/SampleExtraction/TimeSeriesOutputDateFile.txt").astype(str)
date_out = pd.DatetimeIndex([dt.datetime(int(t[:4]), int(t[4:6]), int(t[6:8])) \
                             for t in timeSample])

# Plot samples
np.random.seed(0)
NSAMPLES=100
FRAC = NSAMPLES / SITS.shape[0].compute()

s = SITS.sample(frac=FRAC, replace=False, random_state=0).compute()
m = MASK.loc[s.index.values][band_mask].compute()
sg = GP.loc[s.index.values][band_blue_gp].compute()
mn = MASK_NOISE.loc[s.index.values][band_mask].compute()
sgn = GP_NOISE.loc[s.index.values][band_blue_gp].compute()

with PdfPages("visu_samples_{}_dask.pdf".format(band_dic[BAND])) as pdf:
    for i in s.index:
        fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
                               figsize=(10,5))
        # No Noise
        axs[0].grid()
        axs[0].plot(date_out, sg.loc[i, :],'*r')
        t = (s.loc[i, :] > 0)
        axs[0].set_title("No mask noise")
        axs[0].scatter(date_in[t], s.loc[i].values[t], c=m.loc[i].values[t])
        axs[0].legend(["GapFilled", "Raw"])

        # Noise
        axs[1].grid()
        axs[1].plot(date_out, sgn.loc[i, :],'*r')
        t = (s.loc[i, :] > 0)
        axs[1].set_title("Mask noise")
        axs[1].scatter(date_in[t], s.loc[i].values[t], c=mn.loc[i].values[t])
        axs[1].legend(["GapFilled", "Raw"])

        fig.suptitle("Sample: {}".format(i))
        pdf.savefig()
        plt.close()

    # Write Metadata
    d = pdf.infodict()
    d['Title'] = 'Extracted samples for TGRS papers'
    d['Author'] = 'Mathieu Fauvel'
    d['CreationDate'] = dt.datetime.today()
# Compute mean vector w/wo noise
with PdfPages('visu_means_{}.pdf'.format(band_dic[BAND])) as pdf:
    for cl in [ 1,  2,  3,  4,  5,  6,  7,  8,
                9, 10, 12, 13, 14, 15, 16, 17,
                18, 19, 23]:

        # Load data
        s = SITS.loc[SITS.code==cl][band_blue].compute()
        s[s < 0] = np.nan
        sc = s.copy()

        m = 1 - MASK.loc[s.index.values][band_mask].compute()
        sc[m.values == 0] = np.nan
        
        sg = GP.loc[s.index.values][band_blue_gp].compute()
        sgn = GP_NOISE.loc[s.index.values][band_blue_gp].compute()

        mn = 1 - MASK_NOISE.loc[s.index.values][band_mask].compute()
        sn = s.copy()
        sn[mn.values ==0] = np.nan

        # Compute mean
        mean_raw = np.nanmean(sc.values, axis=0)
        mean_gp = sg.mean(axis=0)
        mean_raw_noise = np.nanmean(sn.values, axis=0)
        mean_gp_noise = sgn.mean(axis=0)

        # Plot mean
        fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
                                figsize=(10,5))
        # No Noise
        axs[0].grid()
        t = mean_raw > 0
        axs[0].plot(date_in[t], mean_raw[t])
        axs[0].plot(date_out, mean_gp)
        axs[0].legend(["Raw", "GapFilled"])

        # Noise
        axs[1].grid()
        t = mean_raw > 0
        axs[1].plot(date_in[t], mean_raw[t])
        axs[1].plot(date_out, mean_gp)
        axs[1].legend(["Raw", "GapFilled"])

    # Write Metadata
    d = pdf.infodict()
    d['Title'] = 'Mean class samples for TGRS papers'
    d['Author'] = 'Mathieu Fauvel'
    d['CreationDate'] = dt.datetime.today()
