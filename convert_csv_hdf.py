import pandas as pd
import glob
import os

TILES = ["T31TCJ", "T31TDN", "T31TGK"]

for TILE in TILES:
    # TILE - Mask
    CSV_FILES = glob.glob("/work/scratch/fauvelm/SampleExtraction/"
                          "outputs/{}/mask_*.csv".format(TILE))
    for csv_file in CSV_FILES:
        print("Process file: {}".format(os.path.basename(csv_file)))
        # Change extension
        hdf_file = csv_file[:-3]+"hdf"

        # Conversion
        pd.read_csv(csv_file).to_hdf(hdf_file,'output',  mode='w', format='table')

        # Delete CSV
        os.remove(csv_file)

    # TILE - sits & GP
    CSV_FILES = glob.glob("/work/scratch/fauvelm/SampleExtraction/"
                          "outputs/{}/sits_*.csv".format(TILE))
    for csv_file in CSV_FILES:
        print("Process file: {}".format(os.path.basename(csv_file)))
        # Change extension
        hdf_file = csv_file[:-3]+"hdf"

        # Conversion
        pd.read_csv(csv_file).to_hdf(hdf_file,'output',  mode='w', format='table')

        # Delete CSV
        os.remove(csv_file)
